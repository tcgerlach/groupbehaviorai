package com.talixa.groupbehavior.ai.behavior;

import com.talixa.groupbehavior.ai.boid.Boid;
import com.talixa.groupbehavior.math.Vector2D;

// behavior to keep boids within a particular range
public class StayWithinCircleBehavior extends Behavior {

	private Vector2D center;
	private float radius;
	
	public StayWithinCircleBehavior(Vector2D center, float radius) {
		this.center = center;
		this.radius = radius;
	}
	
	@Override
	public void updateBoid(Boid b) {
		float distance = center.getDistance(b.getLocation());
		Vector2D toCenter = Vector2D.subtractVectors(center, b.getLocation());
		if (distance > radius) { 
			Vector2D desiredMoveAdj = Vector2D.scaleVector(toCenter, 1/distance);
			desiredMoveAdj.normalize();			
			desiredMoveAdj.scale(b.getMaxSpeed());	// return as quickly as possible
			b.getDesiredMove().addVector(desiredMoveAdj);
		}
	}
}
