package com.talixa.groupbehavior.ai.behavior;

import java.util.List;

import com.talixa.groupbehavior.ai.boid.Boid;
import com.talixa.groupbehavior.math.Vector2D;

public abstract class Behavior {

	protected float distanceToNearest;
	protected Boid nearestBoid;
	
	public abstract void updateBoid(Boid b);
	
	protected void findNearestBoidInGroup(List<Boid> group, Boid me) {
		float minDistance = 99999;
		
		for(Boid otherBoid : group) {
			// ignore self
			if (!me.equals(otherBoid)) {
				float distance = me.getLocation().getDistance(otherBoid.getLocation());
				if (distance < minDistance) {
					minDistance = distance;
					nearestBoid = otherBoid;
				}				
			}
		}	
		distanceToNearest = minDistance;
	}
	
	protected Vector2D getPositionOfCenter(List<Boid> group) {
		float totalX = 0;
		float totalY = 0;
		float groupSize = group.size();
		
		// check for divide by 0
		if (groupSize == 0) {
			return new Vector2D(0,0);
		}
		
		for(Boid other : group) {
			totalX += other.getLocation().getX();
			totalY += other.getLocation().getY();
		}
		
		return new Vector2D(totalX/groupSize, totalY/groupSize);
	}
	
	protected Vector2D getAverageVelocity(List<Boid> group) {
		// save as center position, but using velocity 
		float totalX = 0;
		float totalY = 0;
		float groupSize = group.size();
		
		// check for divide by 0
		if (groupSize == 0) {
			return new Vector2D(0,0);
		}
		
		for(Boid other : group) {
			totalX += other.getVelocity().getX();
			totalY += other.getVelocity().getY();
		}
		
		return new Vector2D(totalX/groupSize, totalY/groupSize);
	}
}
