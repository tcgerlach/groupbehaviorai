package com.talixa.groupbehavior.ai.behavior;

import com.talixa.groupbehavior.ai.boid.Boid;
import com.talixa.groupbehavior.math.Vector2D;

// Align this boid with the direction of the nearest boid
public class AlignmentBehavior extends Behavior {

	private float turnRate;
	
	/**
	 * Keep boids in a group going in the same direction
	 * @param turnRate max rate of turn
	 */
	public AlignmentBehavior(float turnRate) {
		this.turnRate = turnRate;
	}
	
	@Override
	public void updateBoid(Boid boid) {				
		Vector2D desiredMoveAdj = getAverageVelocity(boid.getGroup());
		desiredMoveAdj.normalize();
		desiredMoveAdj.scale(turnRate);
		boid.getDesiredMove().addVector(desiredMoveAdj);
	}
}
