package com.talixa.groupbehavior.ai.boid;

import java.awt.Color;
import java.awt.Graphics;
import java.util.List;

import com.talixa.groupbehavior.ai.behavior.Behavior;
import com.talixa.groupbehavior.math.Vector2D;
import com.talixa.groupbehavior.shared.GroupBehaviorConstants;

public class Boid {	
	private static final int SIZE = 3;
	private static int idCounter = 0;
	
	private List<Behavior> behaviors;		// behaviors this boid must follow
	private List<Boid> group;				// boids in this group (including self)
	private List<Boid> enemies;				// enemies of this boid
	
	// location, vector, & color of this boid
	private Vector2D location;
	private Vector2D velocity = new Vector2D(0,0);
	private Color c;
	private int id;
	
	// variables to determine movement
	private Vector2D desiredMove = new Vector2D(0,0);
	private float desiredSpeed;
	private float maxSpeed;
	
	public Boid(Color c, float x, float y, float vx, float vy, float desiredSpeed, float maxSpeed) {
		location = new Vector2D(x, y);
		velocity = new Vector2D(vx,vy);
		this.desiredSpeed = desiredSpeed;
		this.maxSpeed = maxSpeed;
		this.c = c;
		id = ++idCounter;
	}
	
	public void setDesiredSpeed(float desiredSpeed) {
		this.desiredSpeed = desiredSpeed;
	}
	
	public float getDesiredSpeed() {
		return desiredSpeed;
	}
	
	public void setBehaviors(List<Behavior> behaviors) {
		this.behaviors = behaviors;
	}
	
	public Vector2D getLocation() {
		return location;
	}
	
	public void setLocation(Vector2D location) {
		this.location = location;
	}
	
	public Vector2D getVelocity() {
		return velocity;
	}
	
	public void setVelocity(Vector2D v) {
		this.velocity = v;	
	}
	
	public float getMaxSpeed() {
		return maxSpeed;
	}
	
	public void setGroup(List<Boid> group) {
		this.group = group;
	}
	
	public List<Boid> getGroup() {
		return group;
	}
	
	public void setEnemies(List<Boid> enemies) {
		this.enemies = enemies;
	}
	
	public List<Boid> getEnemies() {
		return enemies;
	}
	
	public Vector2D getDesiredMove() {
		return desiredMove;
	}
	
	public void setDesiredMove(Vector2D desiredMove) {
		this.desiredMove = desiredMove;
	}
	
	public void update(float time) {
		// determine velocity, and set new position
		Vector2D curVel = new Vector2D(velocity.getX(), velocity.getY());
		curVel.scale(GroupBehaviorConstants.SPEED_FACTOR/time);
		location.addVector(curVel);
		
		// reset desired move
		desiredMove = new Vector2D(0,0);
		
		// determine new direction based on behaviors
		for(Behavior b : behaviors) {
			b.updateBoid(this);
		}
		
		// update velocity with changes from behavior
		velocity.addVector(desiredMove);
		float speed = velocity.getMagnitude();
		if (speed > maxSpeed) {
			velocity.normalize();
			velocity.scale(maxSpeed);
		}
	}
	
	public void draw(Graphics g) {
		g.setColor(c);
		g.fillOval((int)location.getX(), (int)location.getY(), SIZE, SIZE);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Boid other = (Boid) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	@Override 
	public String toString() {
		return location.toString();
	}
}
