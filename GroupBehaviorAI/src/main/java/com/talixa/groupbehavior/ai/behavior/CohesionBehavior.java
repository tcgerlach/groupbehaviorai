package com.talixa.groupbehavior.ai.behavior;

import com.talixa.groupbehavior.ai.boid.Boid;
import com.talixa.groupbehavior.math.Vector2D;

public class CohesionBehavior extends Behavior {

	private float maxTurnRate;
	
	/**
	 * This boid will attempt to stay near the center of the boid group.
	 * Thus, the entire boid group will form one large group
	 */
	public CohesionBehavior(float maxTurnRate) {	
		this.maxTurnRate = maxTurnRate;
	}
	
	@Override
	public void updateBoid(Boid b) {
		Vector2D centerOfMass = getPositionOfCenter(b.getGroup());
		Vector2D desiredMoveAdj = Vector2D.subtractVectors(centerOfMass, b.getLocation());
		desiredMoveAdj.normalize();
		desiredMoveAdj.scale(maxTurnRate);
		b.getDesiredMove().addVector(desiredMoveAdj);
	}
}
