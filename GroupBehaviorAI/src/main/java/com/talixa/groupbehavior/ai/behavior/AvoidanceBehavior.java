package com.talixa.groupbehavior.ai.behavior;

import com.talixa.groupbehavior.ai.boid.Boid;
import com.talixa.groupbehavior.math.Vector2D;

public class AvoidanceBehavior extends Behavior {
	
	private float avoidanceDistance;
	private float avoidanceSpeed;
	
	public AvoidanceBehavior(float avoidanceDistance, float avoidanceSpeed) {
		this.avoidanceDistance = avoidanceDistance;
		this.avoidanceSpeed = avoidanceSpeed;
	}
	
	@Override
	public void updateBoid(Boid boid) {
		findNearestBoidInGroup(boid.getEnemies(), boid);
		
		if (distanceToNearest < avoidanceDistance) {
			Vector2D desiredMoveAdj = Vector2D.subtractVectors(boid.getLocation(), nearestBoid.getLocation());
			desiredMoveAdj.normalize();
			desiredMoveAdj.scale(avoidanceSpeed);
			boid.getDesiredMove().addVector(desiredMoveAdj);
		}
	}
}
