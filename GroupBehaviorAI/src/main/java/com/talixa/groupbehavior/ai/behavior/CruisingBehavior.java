package com.talixa.groupbehavior.ai.behavior;

import java.util.Random;

import com.talixa.groupbehavior.ai.boid.Boid;
import com.talixa.groupbehavior.math.Vector2D;

public class CruisingBehavior extends Behavior {

	private float xMoveLeftChance;
	private float xMoveRightChance;
	private float yMoveUpChance;
	private float yMoveDownChance;
	private float minMove;
	private float maxRateChange;
	private float minRateChange;
	
	private static Random rand = new Random(System.currentTimeMillis());
	
	public CruisingBehavior(float xMoveChance, float yMoveChance, float minMove, float minRateChange, float maxRateChange) {
		xMoveLeftChance = xMoveChance/2;
		xMoveRightChance = xMoveChance;
		yMoveUpChance = (yMoveChance - xMoveChance) / 2 + xMoveChance;
		yMoveDownChance = yMoveChance;
		this.minMove = minMove;
		this.maxRateChange = maxRateChange;
		this.minRateChange = minRateChange;
	}

	@Override
	public void updateBoid(Boid b) {				
		float currentSpeed = b.getVelocity().getMagnitude();
		float percentDesiredSpeed = Math.abs((currentSpeed - b.getDesiredSpeed()) / b.getMaxSpeed());
		float sign = (currentSpeed - b.getDesiredSpeed()) > 0 ? -1 : 1;		
		
		// clamp
		if (percentDesiredSpeed < minRateChange) {
			percentDesiredSpeed = minRateChange;
		} else if (percentDesiredSpeed > maxRateChange) {
			percentDesiredSpeed = maxRateChange;
		}
		
		// determine direction
		Vector2D desiredMoveAdj = new Vector2D(0,0);
		int random = Math.abs(rand.nextInt(100));
		if (random < xMoveLeftChance) {
			desiredMoveAdj.setX(minMove*sign*-1);
		} else if (random < xMoveRightChance) {
			desiredMoveAdj.setX(minMove*sign);			
		} else if (random < yMoveUpChance) {
			desiredMoveAdj.setY(minMove*sign*-1);
		} else if (random < yMoveDownChance) {
			desiredMoveAdj.setY(minMove*sign);
		}	
		
		// normalize then scale
		desiredMoveAdj.normalize();
		desiredMoveAdj.scale(minRateChange*sign);
		
		b.getDesiredMove().addVector(desiredMoveAdj);		
	}
}
