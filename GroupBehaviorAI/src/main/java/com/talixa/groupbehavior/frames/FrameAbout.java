package com.talixa.groupbehavior.frames;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.border.Border;

import com.talixa.groupbehavior.listeners.ExitActionListener;
import com.talixa.groupbehavior.shared.GroupBehaviorConstants;
import com.talixa.groupbehavior.shared.IconHelper;

public class FrameAbout {
	
	private static final int WIDTH = 425;
	private static final int HEIGHT = 320;
	
	private static final String ABOUT_CONTENTS = 
			"<html><center>Demonstration of Flocking Behavior Algorithms" +
			"<br><br>Version: " + GroupBehaviorConstants.VERSION + 
			"<br>Copyright 2015, Talixa Software</center></html>";
	
	public static void createAndShowGUI(JFrame owner) {
		// Create dialog
		JDialog dialog = new JDialog(owner, GroupBehaviorConstants.TITLE_ABOUT);
		dialog.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		// set icon
		IconHelper.setIcon(dialog);		
		
		// set escape listener
		addEscapeListener(dialog);
		
		// set modality
		dialog.setModal(true);
		
		// create a content holder with a 10 pixel border
		JPanel holder = new JPanel();
		holder.setLayout(new BorderLayout());
		int border = GroupBehaviorConstants.BORDER;
		holder.setBorder(BorderFactory.createEmptyBorder(border, border, border, border));
		dialog.add(holder);
	
		// create components
		ClassLoader cl = IconHelper.class.getClassLoader();	
		JLabel icon = new JLabel(new ImageIcon(cl.getResource("res/icon.jpg")));
		holder.add(icon, BorderLayout.WEST);
		
		JLabel text = new JLabel(ABOUT_CONTENTS);
		Border padding = BorderFactory.createEmptyBorder(10, 10, 10, 10);
		text.setBorder(padding);
		JButton ok = new JButton(GroupBehaviorConstants.LABEL_OK);
		ok.addActionListener(new ExitActionListener(dialog));
				
		// Add to frame
		holder.add(text, BorderLayout.CENTER);
		holder.add(ok, BorderLayout.SOUTH);
				
		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		dialog.setPreferredSize(new Dimension(WIDTH,HEIGHT));
		int left = (screenSize.width/2) - (WIDTH/2);
		int top  = (screenSize.height/2) - (HEIGHT/2);
		dialog.pack();
		dialog.setLocation(left,top);
		dialog.setVisible(true);						
	}	
	
	public static void addEscapeListener(final JDialog dialog) {
	    ActionListener escListener = new ActionListener() {
	        public void actionPerformed(ActionEvent e) {
	            dialog.dispose();
	        }
	    };

	    dialog.getRootPane().registerKeyboardAction(escListener,
	            KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
	            JComponent.WHEN_IN_FOCUSED_WINDOW);
	}
}
