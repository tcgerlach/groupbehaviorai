package com.talixa.groupbehavior.math;

import java.awt.Point;

public class Vector2D {

	private float x;
	private float y;
	
	public Vector2D() {
		// DO NOTHING
	}
	
	public Vector2D(float x, float y) {	
		this.x = x;
		this.y = y;
	}
	
	public float getX() {
		return x;
	}
	
	public void setX(float x) {
		this.x = x;
	}
	
	public float getY() {
		return y;
	}
	
	public void setY(float y) {
		this.y = y;
	}
	
	public Point asPoint() {
		return new Point((int)x,(int)y);
	}
	
	public float getAngle() {
		return (float)Math.atan2(y, x);
	}
	
	public float getMagnitude() {
		return (float)Math.sqrt(x*x+y*y);
	}
	
	public void addVector(Vector2D other) {
		this.x += other.x;
		this.y += other.y;
	}
	
	public static Vector2D subtractVectors(Vector2D lhs, Vector2D rhs) {
		return new Vector2D(lhs.x - rhs.x, lhs.y - rhs.y);
	}
	
	public static Vector2D scaleVector(Vector2D lhs, float rhs) {
		return new Vector2D (lhs.x * rhs, lhs.y * rhs);
	}
	
	public void subtractVector(Vector2D other) {
		this.x -= other.x;
		this.y -= other.y;
	}
	
	public void scale(float scaler) {
		x *= scaler;
		y *= scaler;
	}
	
	public void normalize() {				
		float mag = getMagnitude();
		
		if (mag == 0) {
			return;
		}
		
		this.x /= mag;
		this.y /= mag;
	}
	
	public float dotProduct(Vector2D vector) {
		return this.x * vector.x + this.y * vector.y;
	}
	
	public float getDistance(Vector2D other) {
		float xdif = other.x - this.x;
		float ydif = other.y - this.y;
		return (float)Math.sqrt(xdif * xdif + ydif * ydif);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(x);
		result = prime * result + Float.floatToIntBits(y);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vector2D other = (Vector2D) obj;
		if (Float.floatToIntBits(x) != Float.floatToIntBits(other.x))
			return false;
		if (Float.floatToIntBits(y) != Float.floatToIntBits(other.y))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Vector2D [x=" + x + ", y=" + y + "]";
	}
}
