package com.talixa.groupbehavior.widgets;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.JPanel;

import com.talixa.groupbehavior.ai.behavior.AlignmentBehavior;
import com.talixa.groupbehavior.ai.behavior.AvoidanceBehavior;
import com.talixa.groupbehavior.ai.behavior.Behavior;
import com.talixa.groupbehavior.ai.behavior.CohesionBehavior;
import com.talixa.groupbehavior.ai.behavior.CruisingBehavior;
import com.talixa.groupbehavior.ai.behavior.SeparationBehavior;
import com.talixa.groupbehavior.ai.behavior.StayWithinCircleBehavior;
import com.talixa.groupbehavior.ai.boid.Boid;
import com.talixa.groupbehavior.math.Vector2D;

@SuppressWarnings("serial")
public class BehaviorPanel extends JPanel {
		
	// these flags enable player control
	public boolean up,down,right,left;

	private static final int WIDTH = 600;
	private static final int HEIGHT = 600;
	
	private List<List<Boid>> boidGroups;
	private long lastUpdate = 0;
	private long currentUpdate = 0;
			
	private static Random rand = new Random(System.currentTimeMillis());
	
	public BehaviorPanel() {							
		// everyone must stay in circle
		Behavior stayWithinCircle = new StayWithinCircleBehavior(new Vector2D(WIDTH/2,HEIGHT/2),WIDTH/2);
		
		// first, make behaviors that will be assigned to boids
		List<Behavior> fishBehaviors = new ArrayList<Behavior>();	
		fishBehaviors.add(stayWithinCircle);
		fishBehaviors.add(new AlignmentBehavior(5));
		fishBehaviors.add(new CohesionBehavior(5));
		fishBehaviors.add(new SeparationBehavior(50,8f,10f));		
		fishBehaviors.add(new AvoidanceBehavior(20, 100));
		
		// monster behaviors
		List<Behavior> monsterBehaviors = new ArrayList<Behavior>();
		monsterBehaviors.add(new CruisingBehavior(40, 80, 5, 15, 30));		
		monsterBehaviors.add(stayWithinCircle);
		
		// create boid groups
		int offset = 150;
		List<Boid> boidGroupOrange = createBoidGroup(1,       WIDTH/2,      HEIGHT/2,   50,  50, Color.ORANGE,  60,  50, monsterBehaviors);
		List<Boid> boidGroupGray   = createBoidGroup(5,  WIDTH-offset,        offset,  -50,  50, Color.GRAY,    90,  80, fishBehaviors);
		List<Boid> boidGroupGreen  = createBoidGroup(15, WIDTH-offset, HEIGHT-offset,  -50, -50, Color.GREEN,   80,  70, fishBehaviors);
		List<Boid> boidGroupBlue   = createBoidGroup(10,       offset, HEIGHT-offset,  100, -50, Color.BLUE,    70,  60, fishBehaviors);	
		List<Boid> boidGroupRed    = createBoidGroup(35,       offset,        offset,  100,  50, Color.RED,     60,  50, fishBehaviors);
				
		// Predatory order = Orange -> Grey -> Green -> Blue -> Red
		addEnemies(boidGroupGray,  null,           null,           null,          boidGroupOrange);
		addEnemies(boidGroupGreen, boidGroupGray,  null,           null,          boidGroupOrange);
		addEnemies(boidGroupBlue,  boidGroupGreen, boidGroupGray,  null,          boidGroupOrange);
		addEnemies(boidGroupRed,   boidGroupBlue,  boidGroupGreen, boidGroupGray, boidGroupOrange);				
		
		boidGroups = new ArrayList<List<Boid>>();
		boidGroups.add(boidGroupGreen);
		boidGroups.add(boidGroupBlue);
		boidGroups.add(boidGroupRed);
		boidGroups.add(boidGroupGray);
		boidGroups.add(boidGroupOrange);
	}
	
	private void addEnemies(List<Boid> boidGroup, List<Boid> enemyGroup1, List<Boid> enemyGroup2, List<Boid> enemyGroup3, List<Boid> enemyGroup4) {
		List<Boid> enemyList = new ArrayList<Boid>();
		if (enemyGroup1 != null) {
			enemyList.addAll(enemyGroup1);
		}
		
		if (enemyGroup2 != null) {
			enemyList.addAll(enemyGroup2);
		}
		
		if (enemyGroup3 != null) {
			enemyList.addAll(enemyGroup3);
		}
		
		if (enemyGroup4 != null) {
			enemyList.addAll(enemyGroup4);
		}
		
		for(Boid b : boidGroup) {
			b.setEnemies(enemyList);
		}
	}
	
	private List<Boid> createBoidGroup(int groupSize, int baseX, int baseY, int baseVelX, int baseVelY, Color c, int maxSpeed, int desiredSpeed, List<Behavior> behaviors) {
		// create group
		List<Boid> boidGroup = new ArrayList<Boid>();	

		// create boids in group
		for(int i = 0; i < groupSize; ++i) {			
			float myX = baseX + rand.nextInt() % 25;
			float myY = baseY + rand.nextInt() % 25;
			float myVX = baseVelX + rand.nextInt() % 5;
			float myVY = baseVelY + rand.nextInt() % 5;
			Boid b = new Boid(c, myX, myY, myVX, myVY,maxSpeed, desiredSpeed);
			b.setBehaviors(behaviors);
			boidGroup.add(b);
		}
		
		// go back through and associate group with each boid
		for(Boid b: boidGroup) {
			b.setGroup(boidGroup);
		}
		
		// return new group
		return boidGroup;
	}
	
	@Override
	public void paint(Graphics g) {		
		super.paint(g);
		
		// get time diff
		currentUpdate = System.currentTimeMillis();
		float timeDiff = currentUpdate - lastUpdate;
		
		// clear background
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, WIDTH, HEIGHT);
		
		// update and draw boids
		for(List<Boid> boidGroup : boidGroups) {
			for(Boid b : boidGroup) {
				b.update(timeDiff);
				b.draw(g);
			}
		}
								
		lastUpdate = currentUpdate;
	}		
}
