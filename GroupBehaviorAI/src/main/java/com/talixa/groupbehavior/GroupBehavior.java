package com.talixa.groupbehavior;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

import com.talixa.groupbehavior.frames.FrameAbout;
import com.talixa.groupbehavior.listeners.DefaultWindowListener;
import com.talixa.groupbehavior.listeners.ExitActionListener;
import com.talixa.groupbehavior.shared.GroupBehaviorConstants;
import com.talixa.groupbehavior.shared.IconHelper;
import com.talixa.groupbehavior.widgets.BehaviorPanel;

public class GroupBehavior {

	private static JFrame frame;
	private static BehaviorPanel panel;
	
	private static void createAndShowGUI() {
		frame = new JFrame(GroupBehaviorConstants.TITLE_MAIN);
		
		// set close functionality 
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
		frame.addWindowListener(new DefaultWindowListener());
		
		panel = new BehaviorPanel();
		frame.add(panel);
		
		// set icon
		IconHelper.setIcon(frame);	
													
		// add menus
		addMenus();			
		
		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setPreferredSize(new Dimension(GroupBehaviorConstants.APP_WIDTH,GroupBehaviorConstants.APP_HEIGHT));
		int left = (screenSize.width/2) - (GroupBehaviorConstants.APP_WIDTH/2);
		int top  = (screenSize.height/2) - (GroupBehaviorConstants.APP_HEIGHT/2);
		frame.pack();
		frame.setLocation(left,top);
		frame.setVisible(true);
		
		Thread animationThread = new Thread() {
			@Override
			public void run() {			
				super.run();
				while (true) {
					panel.repaint();
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						// don't care
					}
				}				
			}
		};
		animationThread.start();
		
		frame.addKeyListener(new KeyListener() {								
			@Override
			public void keyTyped(KeyEvent e) {
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {				
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_UP) {
					panel.up = true; panel.right = false; panel.down = false; panel.left = false;
				} else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
					panel.up = false; panel.right = true; panel.down = false; panel.left = false;
				} else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
					panel.up = false; panel.right = false; panel.down = false; panel.left = true;
				} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					panel.up = false; panel.right = false; panel.down = true; panel.left = false;
				} 
			}
		});
	}
			
	private static void addMenus() {
		// Setup file menu
		JMenu fileMenu = new JMenu(GroupBehaviorConstants.MENU_FILE);
		fileMenu.setMnemonic(KeyEvent.VK_F);
			
		JMenuItem exitMenuItem = new JMenuItem(GroupBehaviorConstants.MENU_EXIT);
		exitMenuItem.setMnemonic(KeyEvent.VK_X);
		exitMenuItem.addActionListener(new ExitActionListener(frame));
		fileMenu.add(exitMenuItem);							
				
		//*******************************************************************************
		// Setup help menu
		JMenu helpMenu = new JMenu(GroupBehaviorConstants.MENU_HELP);
		helpMenu.setMnemonic(KeyEvent.VK_H);
		JMenuItem aboutMenuItem = new JMenuItem(GroupBehaviorConstants.MENU_ABOUT);
		aboutMenuItem.setMnemonic(KeyEvent.VK_A);
		aboutMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FrameAbout.createAndShowGUI(frame);				
			}						
		});
		helpMenu.add(aboutMenuItem);

		//*******************************************************************************
		// Add menus to menubar
		JMenuBar menuBar = new JMenuBar();
		menuBar.add(fileMenu);			
		menuBar.add(helpMenu);
		frame.setJMenuBar(menuBar);
	}
		
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {			
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
