package com.talixa.groupbehavior.shared;

public class GroupBehaviorConstants {
	public static final String VERSION = "1.0";
	
	public static final String TITLE_MAIN = "Group Behavior AI";	
	public static final String TITLE_ABOUT = "About " + TITLE_MAIN;
	
	public static final int APP_WIDTH = 615;
	public static final int APP_HEIGHT = 660;
	
	public static final int BORDER = 10;
	public static final int BORDER_SMALL = 5;
	
	public static final String LABEL_OK = "Ok";	
	
	public static final String MENU_FILE = "File";	
	public static final String MENU_EXIT = "Exit";
	public static final String MENU_HELP = "Help";
	public static final String MENU_ABOUT = "About";
	
	public static final String ICON = "res/icon.jpg";
	
	public static final int SPEED_FACTOR = 5;	// increase this to increase the animation rate
}
